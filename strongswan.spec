Name:             strongswan
Version:          5.7.2
Release:          9
Summary:          An OpenSource IPsec-based VPN and TNC solution
License:          GPLv2+
URL:              http://www.strongswan.org/
Source0:          http://download.strongswan.org/strongswan-%{version}.tar.bz2

Patch0:	          strongswan-multiple-definition.patch
Patch1:           fix-use-of-uninitialized-value.patch

BuildRequires:    gcc systemd-devel gmp-devel libcurl-devel NetworkManager-libnm-devel openldap-devel
BuildRequires:    openssl-devel sqlite-devel gettext-devel trousers-devel libxml2-devel pam-devel
BuildRequires:    json-c-devel libgcrypt-devel systemd-devel iptables-devel chrpath
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd

%description
The strongSwan IPsec implementation supports both the IKEv1 and IKEv2 key exchange
protocols in conjunction with the native NETKEY IPsec stack of the Linux kernel.

%package libipsec
Summary: Strongswan's libipsec backend
%description libipsec
The kernel-libipsec plugin provides an IPsec backend that works entirely in userland, using TUN devices and its own IPsec implementation libipsec.

%package charon-nm
Summary:NetworkManager plugin for Strongswan
Requires:dbus
Obsoletes:      %{name}-NetworkManager < 0:5.0.4-5
Conflicts:      %{name}-NetworkManager < 0:5.0.4-5
Conflicts:      NetworkManager-strongswan < 1.4.2-1

%description charon-nm
NetworkManager plugin integrates a subset of Strongswan capabilities to NetworkManager.

%package sqlite
Summary: SQLite support for strongSwan
Requires: %{name} = %{version}-%{release}

%description sqlite
The sqlite plugin adds an SQLite database backend to strongSwan.

%package tnc-imcvs
Summary: Trusted network connect (TNC)'s IMC/IMV functionality
Requires: %{name} = %{version}-%{release}
Requires: %{name}-sqlite = %{version}-%{release}

%description tnc-imcvs
This package provides Trusted Network Connect's (TNC) architec ture support.
It includes support for TNC client and server (IF-TNCCS), IMC and IMV message
exchange (IF-M), interface between IMC/IMV and TNC client/server (IF-IMC
and IF-IMV). It also includes PTS based IMC/IMV for TPM based remote
attestation, SWID IMC/IMV, and OS IMC/IMV. It's IMC/IMV dynamic libraries
modules can be used by any third party TNC Client/Server imple mentation
possessing a standard IF-IMC/IMV interface. In addition, it im plements
PT-TLS to support TNC over TLS.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-static --with-ipsec-script=strongswan --sysconfdir=%{_sysconfdir}/strongswan \
           --with-ipsecdir=%{_libexecdir}/strongswan --bindir=%{_libexecdir}/strongswan \
           --with-ipseclibdir=%{_libdir}/strongswan --with-fips-mode=2 --enable-bypass-lan \
           --enable-tss-trousers --enable-nm --enable-systemd --enable-openssl --enable-unity \
           --enable-ctr --enable-ccm --enable-gcm --enable-chapoly --enable-md4 --enable-gcrypt \
           --enable-newhope --enable-xauth-eap --enable-xauth-pam --enable-xauth-noauth \
           --enable-eap-identity --enable-eap-md5 --enable-eap-gtc --enable-eap-tls --enable-eap-ttls \
           --enable-eap-peap --enable-eap-mschapv2 --enable-eap-tnc --enable-eap-sim --enable-eap-sim-file \
           --enable-eap-aka --enable-eap-aka-3gpp --enable-eap-aka-3gpp2 --enable-eap-dynamic --enable-eap-radius \
           --enable-ext-auth --enable-ipseckey --enable-pkcs11 --enable-tpm --enable-farp --enable-dhcp \
           --enable-ha --enable-led --enable-sql --enable-sqlite --enable-tnc-ifmap --enable-tnc-pdp \
           --enable-tnc-imc --enable-tnc-imv --enable-tnccs-20 --enable-tnccs-11 --enable-tnccs-dynamic \
           --enable-imc-test --enable-imv-test --enable-imc-scanner --enable-imv-scanner --enable-imc-attestation \
           --enable-imv-attestation --enable-imv-os --enable-imc-os --enable-imc-swid --enable-imv-swid \
           --enable-imc-swima --enable-imv-swima --enable-imc-hcd --enable-imv-hcd --enable-curl \
           --enable-cmd --enable-acert --enable-aikgen --enable-vici --enable-swanctl --enable-duplicheck \
           --enable-kernel-libipsec \
%ifarch x86_64 %{ix86}
           --enable-aesni
%endif

for p in bypass-lan; do
    echo -e "\ncharon.plugins.${p}.load := no" >> conf/plugins/${p}.opt
done

make %{?_smp_mflags}

%install
%make_install

mv %{buildroot}%{_sysconfdir}/strongswan/dbus-1 %{buildroot}%{_sysconfdir}/
# prefix man pages
for i in %{buildroot}%{_mandir}/*/*; do
    if echo "$i" | grep -vq '/strongswan[^\/]*$'; then
        mv "$i" "`echo "$i" | sed -re 's|/([^/]+)$|/strongswan_\1|'`"
    fi
done

rm -rf %{buildroot}%{_libdir}/strongswan/*.so

chmod 644 %{buildroot}%{_sysconfdir}/strongswan/strongswan.conf
install -d -m 700 %{buildroot}%{_sysconfdir}/strongswan/ipsec.d
install -d -m 700 %{buildroot}%{_sysconfdir}/strongswan/ipsec.d/{aacerts acerts cacerts certs crls ocspcerts private reqs}

%delete_la

cd $RPM_BUILD_ROOT/usr
file `find -type f` | grep -w ELF | awk -F":" '{print $1}' | for i in `xargs`
do
chrpath -d $i
done
cd -
mkdir -p %{buildroot}/etc/ld.so.conf.d
echo "%{_libdir}/strongswan" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf

%preun
%systemd_preun strongswan.service

%post
/sbin/ldconfig
%systemd_post strongswan.service

%postun
/sbin/ldconfig
%systemd_postun_with_restart strongswan.service

%files
%doc README NEWS TODO ChangeLog
%license COPYING
%dir %attr(0700,root,root) %{_sysconfdir}/strongswan
%config(noreplace) %{_sysconfdir}/strongswan/*
%dir %{_libdir}/strongswan
%exclude %{_libdir}/strongswan/imcvs
%dir %{_libdir}/strongswan/plugins
%dir %{_libexecdir}/strongswan
%{_unitdir}/strongswan.service
%{_unitdir}/strongswan-swanctl.service
%{_sbindir}/charon-cmd
%{_sbindir}/charon-systemd
%{_sbindir}/strongswan
%{_sbindir}/swanctl
%{_libdir}/strongswan/*.so.*
%exclude %{_libdir}/strongswan/libimcv.so.*
%exclude %{_libdir}/strongswan/libtnccs.so.*
%exclude %{_libdir}/strongswan/libipsec.so.*
%{_libdir}/strongswan/plugins/*.so
%exclude %{_libdir}/strongswan/plugins/libstrongswan-sqlite.so
%exclude %{_libdir}/strongswan/plugins/libstrongswan-*tnc*.so
%exclude %{_libdir}/strongswan/plugins/libstrongswan-kernel-libipsec.so
%{_libexecdir}/strongswan/*
%exclude %{_libexecdir}/strongswan/attest
%exclude %{_libexecdir}/strongswan/pt-tls-client
%exclude %{_libexecdir}/strongswan/charon-nm
%exclude %dir %{_datadir}/strongswan/swidtag
%{_mandir}/man?/*.gz
%{_datadir}/strongswan/templates/config/
%{_datadir}/strongswan/templates/database/
%config(noreplace) /etc/ld.so.conf.d/*

%files sqlite
%{_libdir}/strongswan/plugins/libstrongswan-sqlite.so

%files tnc-imcvs
%{_sbindir}/sw-collector
%{_sbindir}/sec-updater
%dir %{_libdir}/strongswan/imcvs
%dir %{_libdir}/strongswan/plugins
%{_libdir}/strongswan/libimcv.so.*
%{_libdir}/strongswan/libtnccs.so.*
%{_libdir}/strongswan/plugins/libstrongswan-*tnc*.so
%{_libexecdir}/strongswan/attest
%{_libexecdir}/strongswan/pt-tls-client
%dir %{_datadir}/strongswan/swidtag
%{_datadir}/strongswan/swidtag/*.swidtag

%files libipsec
%{_libdir}/strongswan/libipsec.so.*
%{_libdir}/strongswan/plugins/libstrongswan-kernel-libipsec.so

%files charon-nm
%doc COPYING
%{_sysconfdir}/dbus-1/system.d/nm-strongswan-service.conf
%{_libexecdir}/strongswan/charon-nm

%changelog
* Thu Sep 09 2021 caodongxia <caodongxia@huawei.com> - 5.7.2-9
- fix rpath error

* Wed Sep 1 2021 caodongxia <caodongxia@huawei.com> - 5.7.2-8
- fix fuzz: use of uninitialized value

* Mon Aug 4 2021 shdluan <shdluan@163.com> - 5.7.2-7
- fix multiple defination of variable

* Sat Jul 18 2020 yaokai13 <yaokai13@huawei.com> - 5.7.2-6
- Unpack the merged package to fix the issue #l1N2UN

* Thu May 28 2020 Senlin Xia <xiasenlin1@huawei.com> - 5.7.2-5
- prefix man pages
 
* Fri Feb 14 2020 Ling Yang <lingyang2@huawei.com> - 5.7.2-4
- Package init
